class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.string :slug
      t.float :price, default: 0.0

      t.timestamps
    end
  end
end
