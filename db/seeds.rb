CATEGORIES_COUNT = 10
ITEMS_COUNT = 100

puts 'Start Clearing Database'
ItemCategory.all.destroy_all
Category.all.destroy_all
Item.all.destroy_all
puts 'Database is cleared'

puts 'Seeding is started'
categories = FactoryBot.create_list(:category, CATEGORIES_COUNT)
items = FactoryBot.create_list(:item, ITEMS_COUNT)

items.each do |item|
  item.categories << categories.shuffle.take(rand(1..5))
end
puts 'Seeding is done'
