class Item < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: %i[slugged finders]

  has_many :item_categories
  has_many :categories, through: :item_categories
end
