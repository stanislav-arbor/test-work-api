class ItemSerializer < ActiveModel::Serializer
  attributes :id, :slug, :name, :price

  has_many :categories
end
