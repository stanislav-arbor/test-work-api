class Api::ItemsController < ActionController::API
  def index
    @items = Item.all.includes(:categories)
    render json: @items
  end

  def show
    @item = Item.find(params[:id])
    render json: @item
  end
end
