source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# CORE
gem 'rails', '~> 5.1.5'
gem 'dotenv-rails'

# DATABASE
gem 'pg', '>= 0.18', '< 2.0'

# SERVER
gem 'puma', '~> 3.7'
gem 'rack-cors'

# MODELS
gem 'friendly_id', '~> 5.2.1'

# CONTROLLERS
gem 'active_model_serializers', '~> 0.10.0'

group :development, :test do
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'pry-byebug'
  gem 'pry-rails'
end

group :test do
  gem 'database_cleaner'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
end
