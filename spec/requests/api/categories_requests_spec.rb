describe 'Api::CategoriesController', type: :request do
  describe '#GET api/categories' do
    subject(:perform_request) { get '/api/categories' }

    before do
      create_list(:category, 2)
      perform_request
    end

    it { expect(json_response.count).to eq(2) }
    it_behaves_like 'checking base response data'
  end
end
