describe 'Api::ItemsController', type: :request do
  describe '#GET api/items' do
    subject(:perform_request) { get '/api/items' }

    before do
      create_list(:item, 2)
      perform_request
    end

    it { expect(json_response.count).to eq(2) }
    it_behaves_like 'checking base response data'
  end

  describe '#GET api/items/:id' do
    subject(:perform_request) { get '/api/items', params: { id: item.id } }
    let(:item) { create(:item) }

    before { perform_request }

    it { expect(json_response.first['id']).to eq(item.id) }
    it_behaves_like 'checking base response data'
  end
end
