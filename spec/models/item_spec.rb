describe Item, type: :model do
  it { should have_many(:item_categories) }
  it { should have_many(:categories).through(:item_categories) }
end
