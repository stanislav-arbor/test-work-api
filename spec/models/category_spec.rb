describe Category, type: :model do
  it { should have_many(:item_categories) }
  it { should have_many(:items).through(:item_categories) }
end
