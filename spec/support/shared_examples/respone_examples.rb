shared_examples 'checking base response data' do
  it { expect(response.content_type).to eq('application/json') }
  it { expect(response).to have_http_status(200) }
end
