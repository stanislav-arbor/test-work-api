FactoryBot.define do
  factory :item do
    name { Faker::Commerce.product_name }
    price { Faker::Commerce.price }

    trait :with_single_category do
      after(:create) do |item|
        FactoryBot.create(:item_category, item: item)
      end
    end

    trait :with_multiple_categories do
      after(:create) do |item|
        FactoryBot.create_list(:item_category, rand(2..5), item: item)
      end
    end
  end
end
