FactoryBot.define do
  factory :item_category do
    category
    item
  end
end
