FROM ruby:2.4.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /itemer

WORKDIR /itemer

COPY Gemfile /itemer/Gemfile
COPY Gemfile.lock /itemer/Gemfile.lock

RUN bundle install

COPY . /itemer
