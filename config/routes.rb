Rails.application.routes.draw do
  namespace :api do
    resources :items, only: %i[index show]
    resources :categories, only: [:index]
  end
end
