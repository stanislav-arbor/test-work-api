Rails.application.config.middleware.insert_before 0, Rack::Cors do
  methods = %i[get post put patch delete options head]

  allow do
    origins '*'
    resource '*', headers: :any, methods: methods
  end
end
