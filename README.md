# Rails 5 API
## Introduction
Simple api application for [angular 5 client](https://bitbucket.org/stanislav-arbor/test-work-client)

## Install
#### With docker
In [this repository](https://bitbucket.org/stanislav-arbor/test-work-doker) you can find **docker-compose** file
for run rails and angular together. More details about run with docker is
[here](https://bitbucket.org/stanislav-arbor/test-work-doker)

#### Without docker
```
# If you have not this gem yet
gem install bundler

# Install dependencies
bundle install
```
First of all we need to copy `.env.example` with name
`.env` and fill database credentials
in rails folder run following commands
```
rails db:create && rails db:migrate && rails db:seed
```
After this command you can run servers
```
rails s -p 3000 -b '0.0.0.0'
```
If no errors was raised, then all done. You can use endpoint
and use angular application

## Routes
Available routes:

  - 'api/items' - get all items
  - 'api/items/:id' - get single item by slug or id
  - 'api/categories' - get all categories

Endpoints:

- Angular: `localhost:4200`
- Rails: `localhost:3000`
